#!/usr/bin/env python3

# Copyright © 2016 Hraban Luyat <hraban@0brg.net>
#
# This source code is licensed under the AGPLv3. Details in the LICENSE file.

import sys
import os
import itertools as it
import logging
from io import BytesIO
from PyPDF2 import PdfFileMerger, PdfFileReader

def getadmindir():
    if len(sys.argv) == 1:
        return os.getcwd()
    else:
        return sys.argv[1]

def ls(dirname):
    """Sorted listing of directory with full path prefix in output"""
    return map(lambda x: os.path.join(dirname, x), sorted(os.listdir(dirname)))

def isimage(fname):
    return any(map(fname.endswith, ['.jpg', '.png']))

def topdf(handler):
    from reportlab.pdfgen import canvas
    from reportlab.lib.pagesizes import A4
    packet = BytesIO()
    c = canvas.Canvas(packet, pagesize=A4)
    handler(c)
    c.save()
    packet.seek(0)
    return packet

def text2pdf(txt):
    from reportlab.lib.units import mm
    def create(c):
        c.setFont("Times-Roman", 50)
        c.drawString(40 * mm, 150 * mm, txt)
        c.showPage()
    return topdf(create)

def image2pdf(fname):
    def create(c):
        c.drawImage(fname, 0, 0)
        c.showPage()
    return topdf(create)

def handle_sub(pdfout, subname, wd):
    logging.debug("Processing %s/%s" % (wd, subname))
    pdfout.append(text2pdf(subname))
    for fname in reversed(list(ls(os.path.join(wd, subname)))):
        logging.debug("    - %s" % (fname,))
        f = None
        if isimage(fname):
            f = image2pdf(fname)
        elif fname.endswith('.pdf'):
            f = open(fname, 'rb')
        else:
            raise ApplicationException("Unrecognized file: " + fname)
        pdfout.append(f)

def main():
    logging.basicConfig(level=logging.DEBUG)
    dirname = getadmindir()

    pdfout = PdfFileMerger()
    for subname in sorted(os.listdir(dirname)):
        handle_sub(pdfout, subname, dirname)

    outf = open(os.path.join(dirname, "administratie.pdf"), "wb")
    pdfout.write(outf)

if __name__ == "__main__":
    main()
