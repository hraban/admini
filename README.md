## admini

Mini administratie bundler voor "Agaath formaat".

## Gebruik

Docker:

```
$ docker run --rm -v /pad/naar/pdfs:/administratie registry.gitlab.com/hraban/admini 
```

Plaatst een administratie.pdf file in de root van de administratie directory.

## Structuur van administratie

De administratie folder moet bestaan uit enkel folders:

- debi
- credi
- kas
- bank

De namen zijn slechts indicatief.

In elk van deze folders mogen alleen .pdf, .png of .jpg bestanden leven. Ze
worden gesorteerd op naam, in reverse order (volgens Agaath formaat). Dus
gebruik de datum als prefix. E.g., in debi:

- factuur 1 klantA.pdf
- factuur 2 klantB.pdf
- etc..

in credi:

- 2016-08-01 hosting.pdf
- 2016-08-04 conferentie tickets.pdf
- etc...
